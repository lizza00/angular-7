import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  cars = [
    {
      name: 'Mersedes',
      price: 1213,
      complecity: `full`
    },
    {
      name: 'Subaru',
      price: 213,
      complecity: `half`
    }
  ];

  addCar(car) {
    this.cars.push(car);
  }

  removeCar(index) {
    this.cars.splice(index, 1);
  }
}
