import {Component, OnInit, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-new-car',
  templateUrl: './new-car.component.html',
  styleUrls: ['./new-car.component.scss']
})
export class NewCarComponent implements OnInit {

  @Output() car = new EventEmitter();
  showForm = false;

  constructor() {
  }

  ngOnInit() {
  }

  onSubmit(myForm) {
    const fields = myForm.form.controls;
    this.showForm = false;
    this.car.emit({
      name: fields.name.value,
      price: fields.price.value,
      complecity: fields.complecity.value
    });
  }

}
