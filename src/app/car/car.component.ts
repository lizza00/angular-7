import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.scss']
})
export class CarComponent implements OnInit {

  showInfo = false;
  @Input() car;
  @Input() carIndex;
  @Output() removedCar = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  removeCar() {
    this.removedCar.emit(this.carIndex);
  }
}
